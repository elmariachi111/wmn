import React from "react";
import bgDoodle from '../images/funky-lines.png'

export default function Header() {
  return (
    <header className="h-20" style={{backgroundImage: `url(${bgDoodle})`}}></header>
  );
}
