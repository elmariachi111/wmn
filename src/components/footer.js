import React from "react";
import bgDoodle from '../images/regal.png'
import {Link, useStaticQuery, graphql} from 'gatsby'

export default function Footer() {
  const data = useStaticQuery(graphql`
  {
    site {
      siteMetadata {
        email
      }
    }
  }
  `)
  return (
    <footer style={{backgroundImage: `url(${bgDoodle})`}}>
        <nav className="flex justify-evenly max-w-4xl mx-auto p-4 md:p-8 text-sm">
          <p className="text-lg">
            <Link to="/imprint">imprint</Link>
          </p>
         
          <p className="text-lg">
            <Link to="/">home</Link>

          </p>
          <p className="text-lg">
            <a href={`mailto:${data.site.siteMetadata.email}`}>contact</a>
          </p>
        </nav>
    </footer>
  );
}
