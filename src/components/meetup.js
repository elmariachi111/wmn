/* eslint-disable react/prop-types */
import React from 'react'

function Meetup({ name, local_date, link,status }) {
    return (
        <div className="mb-8">
            <time className="block">{local_date}</time> 
            <h3 className="text-2xl">
                <a href={link} target="_blank" rel="noopener noreferrer">{name}</a>
            </h3>
            {(status == 'upcoming') &&
                <a href={link} >&rsaquo; RSVP</a>
            }
        </div>
    )
}

export default Meetup

//{/* <a href={link} className="bg-red-400 hover:bg-red-500 text-white py-1 px-2 rounded-full">RSVP</a>*/}