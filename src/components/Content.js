/* eslint-disable react/prop-types */
import React from 'react'

export default function Content({children}) {
return (
    <section className="text-4xl mx-auto px-4">
        {children}
    </section>
    )
}