module.exports = {
  siteMetadata: {
    title: `WMN - Was mit Networking`,
    description: `WMN -WMN wants to raise awareness and create a feeling of togetherness by providing events to establish and maintain connections, take part in inspiring discussions and celebrate integrity.`,
    author: `@stadolf`,
    email: `info@wmn.digital`
  },
  plugins: [
    "gatsby-plugin-eslint",
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
        publicPath: 'panel'
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-tailwind`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#4dc0b5`,
        display: `minimal-ui`,
        icon: `src/images/tailwind-icon.png`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/data/`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        commonmark: true,
      }
    },
    {
      resolve: `gatsby-source-meetup`,
      options: {
        groupUrlName: "WMN-Was-mit-Networking",
        status: "upcoming,past",
        desc: "true",
        page: 10
      }
    },
    {
      resolve: "gatsby-plugin-postcss",
      options: {
        postCssPlugins: [
          require(`tailwindcss`)(`./tailwind.config.js`),
          require(`autoprefixer`),
          require(`cssnano`)
        ]
      }
    },
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        tailwind: true,
        purgeOnly: [`src/css/style.css`],
        whitelist: ['ul']
      }
    },
    //`gatsby-plugin-offline`
    
  ]
};
