---
title: Impressum
slug: imprint
---
# Impressum

WMN is a free and liberal community powered by many people. In case you need to approach someone because of legal reasons, use this contact. If you just want to drop us a message use the contact mail in our footer.

**WMN**

Ohlauer Straße 43\
10999 Berlin

+49 30 2847 2640 0\
info@wmn.digital

## Ansprechpartner*innen

Sarah Hoidn\
Lena Lidl\
Thomas Ullner

Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV: Sarah Hoidn, Ohlauer Straße 43, 10999 Berlin
