---
title: Home
slug: home
---
The foundation of WMN relies on the idea of connecting different people, perspectives and projects. Aiming to provide a platform for professional, social, economic advancement especially for women and non-binary people within the tech world. 

WMN raises awareness and creates togetherness - a supporting network for all genders and sexualities in the LGBTQ+ community - providing events to establish and maintain connections.
Take part in inspiring discussions and enjoy the occasional glass of wine (or sparkling water) as we celebrate integrity.
We offer you an inspirational exchange, with our intelligent, open-minded community.
Connect, create and share.

### WMN is

_open for everyone\
_international and diverse\
_kind and intelligent\
_community driven\
_Berlin based\
_waiting for you
